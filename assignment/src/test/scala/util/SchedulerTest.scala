package util

import java.util.concurrent.atomic.AtomicBoolean
import scala.concurrent.duration._
import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class SchedulerTest extends FlatSpec {
  "scheduler" should "execute block after a delay" in {
    val complete = new AtomicBoolean()
    Scheduler.executeAfter(1.second)(complete.set(true))

    Thread.sleep(200)
    complete.get shouldBe false

    Thread.sleep(1500)
    complete.get shouldBe true
  }
}
